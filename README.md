<a id="educajou" href="../"><img class="logo" src="https://educajou.forge.apps.education.fr/img/educajou.png"></a>

<img class="logo" src="https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/images/logo.png">

Tuxblocs est un logiciel qui permet de représenter les milliers, centaines, dizaines et unités sous fomes de blocs de base, et d'effectuer des conversions entre colonnes.
Il aide à comprendre le système de numération positionnelle.
Ce n'est pas un exerciseur autonome pour l'élève. Il s'agit d'un outil de représentation, au même titre que le matériel de manipulation que l'on trouve dans les classes.

<a href="https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/paquets/tuxblocs_4.0.stable_all.deb" target="_blank">
<span class="bouton">
📥 Télécharger Tuxblocs pour Linux (DEB)
</span>
</a>

<a href="https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/paquets/Tuxblocs_installeur_v_4-0.exe" target="_blank">
<span class="bouton">
📥 Télécharger Tuxblocs pour Windows (10 et +)
</span>
</a>


<a href="https://educajou.forge.apps.education.fr/tuxblocs-html" target="_blank">
<span class="bouton">
🌐 Tuxblocs en ligne (en construction)
</span>
</a>

[🧐 Information importante pour l'installation sous Windows](https://educajou.forge.apps.education.fr/tuxblocs/#windows-10-11-64-bits)



## Aperçu

![](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/illustration.png)


<iframe title="01 Prise en main" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/0ca2ba99-b1f1-45f1-97de-8ba4a69bc35c" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>


<iframe title="02 Le mode marqueur" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/ebb9874c-9b57-460c-aa80-20d4520c7650" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

## Téléchargement et installation

### Windows

Télécharger [l'installeur Windows](https://lstu.fr/tuxblocs_4_windows).


### Ubuntu, Linux Mint, Debian

Télécharger [le paquet DEB](https://lstu.fr/tuxblocs_4_linux).


## Installation

### Linux (deb : Ubuntu, Debian, Linux Mint, Primtux ...)

Sous Linux, ouvrir le paquet deb via le gestionnaire de paquets habituel (logithèque, gdebi...).

### Windows (10 & 11, 64 bits)

Exécuter le programme d'installation.

Une fenêtre Windows affiche un avertissement car le logiciel n'est pas signé avec un certificat Microsoft.

> Cliquer sur `Informations complémentaires` et `Éxécuter quand même`.
> <div> <a target="_blank" href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/InfoComp.png"><img alt="Informations complémentaires" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/InfoComp.png" width="25%;"></a> → <a target="_blank" href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Executer.png"><img alt="Executer quand même" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Executer.png" width="25%;"></a> </div>


## Utilisation

### Tutoriels

#### Vidéos

[Chaîne Peertube sur Numérique Éducatif](https://tube-numerique-educatif.apps.education.fr/c/tuxblocs/videos)


#### Documents

![](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/docs.png)

[📄Boutons et raccourcis clavier](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_boutons_et_raccourcis.pdf?inline=false)

[📄Séance CP - Désigner le nombre de paquets de 10 et les objets restants](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_01_CP%20D%C3%A9signer%20nb%20paquets%2010%20objets%20restants.pdf?inline=false)

[📄Séance CP - Désigner les nombres en dizaines / unités](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_02_CP%20D%C3%A9signer%20nb%20dizaines%20et%20unit%C3%A9s.pdf?inline=false)

[📄Séance CP - Réunir deux collections avec retenue](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_03_CP%20R%C3%A9unir%20deux%20collections%20avec%20retenue.pdf?inline=false)

[📄Séance CE1 - Moitié d'un nombre à deux chiffres](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_05_CE1%20Moiti%C3%A9%20nombre%202%20chiffres.pdf?inline=false)

[📄Séance CE2 - Exprimer un nombre à quatre chiffres](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_04_CE2%20Exprimer%20nb%204%20ch.pdf?inline=false)

[📄Séance CE1 - Exprimer un nombre à trois chiffres](https://forge.apps.education.fr/educajou/tuxblocs/-/raw/master/doc/pdf/Tuxblocs_fiche_06_CE1%20Exprimer%20nb%203%20ch.pdf?inline=false)


[🛠️ Sur la forge : versions modifiables des documents](https://forge.apps.education.fr/educajou/tuxblocs/-/tree/master/doc/sources)



#### Manuel

Tuxblocs est prévu pour être piloté à la souris ou au clavier, via de nombreux raccourcis, ou même dans une certaine mesure avec une télécommande de présentation, grace à l'émulation des touches <-- --> et "B" (voir les raccourcis clavier).

Par défaut, le compteur général affiche le nombre total de cubes, du moment qu'aucune colonne ne contient plus de 9 objets. Ce comportement peut être modifié via le gestionnaire d'options.
    
Pour ajouter un millier, une centaine, une dizaine, une unité :
- cliquer sur le bouton + de la colonne correspondante
- ou bien utiliser la molette de la souris en se plaçant dans la colonne souhaitée
- ou bien utiliser les raccourcis clavier : "m,c,d,u"
- ou encore faire glisser un nouvel objet depuis le bouton correspondant (étoile rouge)

Pour supprimer un millier, une centaine, une dizaine, une unité (ou un groupe, si marqué - voir plus bas) :
- cliquer sur le bouton - de la colonne correspondante
- ou bien utiliser la molette de la souris en se plaçant dans la colonne souhaitée
- ou bien utiliser les raccourcis clavier : "ctrl m,, ctr c, ctrl d,ctrl u"
- ou encore faire glisser l'objet en dehors de la zone de travail

NB : si des objets sont marqués, l'utilisation du bouton "-", de la molette de la souris ou du raccourci clavier correspondant aura pour effet de supprimer tous les objets marqués de la colonne.
Par exemple si 3 unités sont marquées, un clic sur le bouton "-" de la colonne des unités les supprimera d'un seul coup.

Pour grouper des unités, dizaines ou centaines :
- cliquer sur le bouton "grouper" situé en haut de la colonne.
NB : si des objets sont marqués, ils ne seront groupés qu'à condition d'être 10.
- ou bien dessiner un rectangle englobant au moins 10 objets

Pour dégrouper des milliers, centaines ou dizaines :
- cliquer sur le bouton "dégrouper" situé en haut de la colonne.
- ou bien double-cliquer sur l'objet.

Pour déplacer un groupe vers la colonne de gauche :
- utiliser le bouton <<< situé en bas.
- ou bien le faire glisser dans la colonne de gauche

Pour casser un millier, une centaine ou une dizaine :
- cliquer sur le bouton >>> en bas de la colonne correspondante.
- ou bien double-cliquer sur l'objet.
- ou encore le faire glisser dans la colonne de droite.

Pour déplacer un objet, il suffit de cliquer dessus, de maintenir le bouton de la souris enfoncé et de déplacer le pointeur, en mode "déposer - glisser". Ce fonctionnement est inhibé en mode "marqueur" (voir plus bas).

Pour marquer des objets, maintenir la touche "Maj" et cliquer sur un objet ou dessiner un rectangle autour d'un groupe d'objets.
Il est possible également de basculer en mode "marqueur" via le bouton dédié (représentant une unité encadrée de rouge) ou encore par le raccourci clavier "S". La touche "Maj" fonctionnera alors dans l'autre sens, permettant de réaliser ponctuellement les actions du mode "normal".
Les objets marqués peuvent être supprimés au moyen du bouton "-" (moins) de la colonne correspondante, ou bien via le raccourci clavier "Suppr".

Pour masquer un compteur particulier ou le compteur général, cliquer sur le bouton en forme d'oeil. Il n'est pas possible de masquer le compteur général en mode "interrogation".
Pour masquer ou montrer tous les compteurs, utiliser la touche "V" (voir) du clavier.

Pour remettre à zéro une colonne ou l'ensemble, utiliser les boutons corbeille. Raccourci clavier : touche Espace pour la corbeille générale.

Pour verrouiller la somme, cliquer sur le bouton "cadenas" à gauche. Cela a pour effet d'empêcher toute création ou suppression ayant pour effet de modifier le nombre total. Les remises à zéro (corbeille), modifications du nombre de colonnes, tirage aléatoire et bouton "quitter" sont également désactivés.
En mode "interrogation", le verrouillage n'empêche pas de modifier le nombre :  seulement de changer de mode ou de tirer un nouveau nombre.

Le bouton "Au hasard" affiche un nombre aléatoire de milliers, centaines, dizaines, unités (selon le nombre de colonnes actives). Les curseurs verticaux permettent de définir un nombre maximum pour chacune.

Pour modifier les couleurs des blocs, cliquer sur le sélecteur situé en bas de chaque colonne.

Pour afficher une séparation horizontale, cliquer sur le bouton "séparations".

Il est possible d'entrer directement un nombre (maximum 9999) dans la zone de saisie située tout à gauche.
- le bouton "afficher" créera les objets correspondants et mettra les compteurs à jour,
- le bouton "interroger" affichera le nombre saisi dans le compteur, (selon options, sans mettre à jour les blocs). L'élève devra alors réaliser les modifications nécessaires puis cliquer suer le point d'interrogation situé sur le compteur pour valider.

Pour quitter le mode interroger :
- placer la bonne quantité de blocs et valider avec "OK"
- ou utiliser le bouton rouge "x" (désactivé lors du verrouillage)
- ou effectuer un changement de mode (colonnes ou blocs aléatoires) (désactivé lors du verrouillage)

Raccourcis clavier :
    
u : ajouter une unité
d : ajouter une dizaine
c: ajouter une centaine
m : ajouter un millier
Ctrl u : supprimer une unité
Ctrl d : supprimer une dizaine
Ctrl c: supprimer une centaine
Ctrl m : supprimer un millier

Barre espace : supprimer tout
Suppr : supprimer les blocs marqués

v : voir / cacher tous les compteurs (sauf compteur principal en mode "interroger")
b : voir / masquer les blocs
r : ranger tous les blocs
t : tirer un nombre au hasard (cache les compteurs)
l : verrouiller / déverrouiller le nombre (empêche d'ajouter ou de supprimer des blocs, désactive les boutons corbeille, hasard et colonnes). En mode "interrogation", le verrouillage empêche seulement de changer de mode ou de tirer un nouveau nombre.
s : basculer le fonctionnement de la souris (mode marqueur / mode normal)
x : multiplier par 10 (hors mode "1 colonne")
Page Down : afficher une quantité de blocs choisie (si la zone de saisie comporte un nombre accepté) ou aléatoire (si la zone de saisie est vide)
Page up : interroger sur le nombre choisi (si la zone de saisie comporte un nombre accepté) ou aléatoire (si la zone de saisie est vide)
Échap : quitter
F1 : aide
F11 : plein écran

## Sources

Le code source de Tuxblocs est disponible sur https://forge.apps.education.fr/educajou/tuxblocs

## Licence

Le logiciel Tuxblocs est sous licence libre GNU/GPL.
Développeur : Arnaud Champollion

Cette documentation ainsi que les vidéos et illustrations sont sous licence Creative Commons BY SA
Contributeurs : Marc Degioanni, Arnaud Champollion

