# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/tuxblocs/issues
# Bug-Submit: https://github.com/<user>/tuxblocs/issues/new
# Changelog: https://github.com/<user>/tuxblocs/blob/master/CHANGES
# Documentation: https://github.com/<user>/tuxblocs/wiki
# Repository-Browse: https://github.com/<user>/tuxblocs
# Repository: https://github.com/<user>/tuxblocs.git
